import * as d3 from 'd3';
import * as dagre from 'dagre';
import {Bbn, BbnNode, EvidenceBuilder, JoinTree} from './graph';
import {InferenceController} from './pptc';

const OUT_LEFT = 1, OUT_TOP = 2, OUT_RIGHT = 4, OUT_BOTTOM = 8;
const MAX_NODE_NAME_LENGTH = 15;
const MAX_VALUE_LENGTH = 5;
const MAX_PCT_VALUE = 6;
const DEFAULT_PADDING = '\u00A0';

export class VGraph {

  constructor() {
    this.nodes = new Map<number, VNode>();
    this.edges = new Map<number, VEdge>();
  }
  nodes: Map<number, VNode>;
  edges: Map<number, VEdge>;

  public static fromBbn(bbn: Bbn, jt: JoinTree): VGraph {
    const g = new VGraph();
    const nodes = new Map<number, VNode>();
    bbn.getNodes().forEach(n => {
      const bbnNode = n as BbnNode;
      const probs = jt.getBbnPotential(bbnNode).entries.map(e => e.value);
      const node = new VNode(bbnNode.id, bbnNode.variable.name, bbnNode.variable.values, probs);

      g.addNode(node);
      nodes.set(node.id, node);
    });
    bbn.getEdges().forEach(e => {
      const paId = e.lhs.id;
      const chId = e.rhs.id;
      const pa = nodes.get(paId);
      const ch = nodes.get(chId);
      const edge = new VEdge(pa, ch);
      g.addEdge(edge);
    });
    return g;
  }

  public getNodes(): VNode[] {
    return Array.from(this.nodes.values());
  }

  public getEdges(): VEdge[] {
    return Array.from(this.edges.values());
  }

  public addNode(node: VNode): VGraph {
    this.nodes.set(node.id, node);
    return this;
  }

  public addEdge(edge: VEdge): VGraph {
    this.edges.set(edge.getId(), edge);
    return this;
  }

  public getNode(id: number): VNode {
    return this.nodes.get(id);
  }

  public getPath(id1: number, id2: number): VPath {
    return this.getNode(id1).getPath(this.getNode(id2));
  }
}

export class VEdge {
  public points: VPoint[];

  constructor(public parent: VNode, public child: VNode) {
    this.points = [];
  }

  public getId(): number {
    return Util.hashOfStr(this.toString());
  }

  public toString(): string {
    return this.parent.label + '->' + this.child.label;
  }
}

export class VNode {
  public width = 150;
  public height: number;
  public point: VPoint;

  constructor(public id: number, public label: string, public values: string[], public probs: number[]) {
    this.probs = this.normalizeProbs(this.probs);
    this.height = values.length * 15 + 20;
    this.point = new VPoint(Math.random(), Math.random());
  }

  public updatePoint(point: VPoint): void {
    this.point.x = point.x;
    this.point.y = point.y;
  }

  public getRefId(): string {
    return this.label;
  }

  public getTranslation(): string {
    return 'translate(' + this.point.x + ',' + this.point.y + ')';
  }

  public getMid(): VPoint {
    const dx = this.width / 2.0 + this.point.x;
    const dy = this.height / 2.0 + this.point.y;
    return new VPoint(dx, dy);
  }

  public normalizeProbs(probs: number[]): number[] {
    const sum = probs.reduce((a, b) => a + b);
    return probs.map(p => p / sum);
  }

  public getPath(that: VNode): VPath {
    const theta = that.point.getTheta(this.point);
    const p1 = this.getPoint(theta);
    const p2 = that.getPoint(theta + Math.PI);
    const error = p1.error || p2.error ? true : false;

    const path = new VPath(p1, p2);
    path.error = error;

    return path;
  }

  public toString(): string {
    return this.id + '|' +
        this.label + '|' +
        this.values.join(',') + '|' +
        this.probs.join(',') + '|' +
        this.point.toString();
  }

  private getPoint(theta: number): VPoint {
    const c = this.getMid();
    const cx = c.x;
    const cy = c.y;
    const w = this.width / 2.0;
    const h = this.height / 2.0;
    const d = this.getDistance(new VPoint(cx, cy), new VPoint(cx + w, cy + h));
    const x = cx + d * Math.cos(theta);
    const y = cy + d * Math.sin(theta);
    const ocode = this.getOutCode(new VPoint(x, y));

    let px = 0;
    let py = 0;
    let error = false;

    switch (ocode) {
      case OUT_TOP:
        px = cx - h * ((x - cx) / (y - cy));
        py = cy - h;
        break;
      case OUT_LEFT:
        px = cx - w;
        py = cy - w * ((y - cy) / (x - cx));
        break;
      case OUT_BOTTOM:
        px = cx + h * ((x - cx) / (y - cy));
        py = cy + h;
        break;
      case OUT_RIGHT:
        px = cx + w;
        py = cy + w * ((y - cy) / (x - cx));
        break;
      default:
        error = true;
    }

    const p = new VPoint(px, py);
    p.error = error;
    return p;
  }

  private getDistance(p1: VPoint, p2: VPoint): number {
    const x = p1.x - p2.x;
    const y = p1.y - p2.y;
    const d = Math.sqrt((x * x) + (y * y));
    return d;
  }

  private getOutCode(point: VPoint): number {
    let out = 0;

    if (this.width <= 0) {
      out |= OUT_LEFT | OUT_RIGHT;
    } else if (point.x < this.point.x) {
      out |= OUT_LEFT;
    } else if (point.x > this.point.x + this.width) {
      out |= OUT_RIGHT;
    }

    if (this.height <= 0) {
      out |= OUT_TOP | OUT_BOTTOM;
    } else if (point.y < this.point.y) {
      out |= OUT_TOP;
    } else if (point.y > this.point.y + this.height) {
      out |= OUT_BOTTOM;
    }

    return out;
  }
}

export class VPoint {
  public error = false;

  constructor(public x: number, public y: number) {

  }

  public toString(): string {
    return '(' + this.x + ',' + this.y + ')';
  }

  public getTheta(that: VPoint): number {
    return Math.atan2(this.y - that.y, this.x - that.x);
  }
}

export class VPath {
  public error = false;

  constructor(public p1: VPoint, public p2: VPoint) {

  }

  public toString(): string {
    return this.p1.toString() + ' to ' + this.p2.toString();
  }
}

export class Util {
  static hashOfStr(s: string): number {
    let hash = 0;
    if (s.length === 0) {
      return hash;
    }

    for (let i = 0, len = s.length; i < len; i++) {
      const chr = s.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }

  static hashOfArr(arr: string[]): number {
    return this.hashOfStr(arr.sort().join('-'));
  }
}

export interface InferenceEngine {
  getGraph(): VGraph;

  updateEvidence(id: number, value: string): void;

  getProbs(id: number): number[];
}

export class NoOpInferenceEngine implements InferenceEngine {
  constructor(public graph: VGraph) {

  }

  public getGraph(): VGraph {
    return this.graph;
  }

  public updateEvidence(id: number, value: string): void {

  }

  public getProbs(id: number): number[] {
    return this.graph.getNode(id).probs;
  }
}

export class BasicInferenceEngine implements InferenceEngine {
  vGraph: VGraph;
  bbn: Bbn;
  jt: JoinTree;

  constructor(bbn: Bbn) {
    this.bbn = bbn;
    this.jt = InferenceController.apply(this.bbn);
    this.vGraph = VGraph.fromBbn(this.bbn, this.jt);
  }

  getGraph(): VGraph {
    return this.vGraph;
  }

  getProbs(id: number): number[] {
    return this.jt.getBbnPotential(this.jt.getBbnNode(id)).entries.map(e => e.value);
  }

  updateEvidence(id: number, value: string): void {
    const e = new EvidenceBuilder()
        .withNode(this.jt.getBbnNode(id))
        .withEvidence(value, 1.0)
        .build();
    this.jt.setObservation(e);
  }

}

export class RenderOptions {
  constructor(public id: string, public width: number, public height: number) {

  }
}

export class GraphRenderer {
  graph: VGraph;

  constructor(public engine: InferenceEngine, public options: RenderOptions) {
    this.graph = this.engine.getGraph();
  }

  public draw(): void {
    this.initSvg();
    this.layoutGraph();
    this.drawEdges();
    this.drawNodes();
  }

  private initSvg(): void {
    d3.select(this.options.id)
        .attr({
          width: this.options.width,
          height: this.options.height
        })
        .append('defs')
        .append('marker')
        .attr({
          id: 'arrow',
          markerWidth: 10,
          markerHeight: 10,
          refX: 5,
          refY: 3,
          orient: 'auto',
          markerUnits: 'strokeWidth'
        })
        .append('path')
        .attr({
          d: 'M0,0 L0,6, L5,3 z',
          fill: '#f00',
          class: 'edge-head'
        });
  }

  private getDagreGraph(): Dagre.Graph {
    const g = new dagre.graphlib.Graph();
    g.setGraph({});
    g.setDefaultEdgeLabel(() => {
      return {};
    });

    this.graph.getNodes()
        .forEach(n => {
          g.setNode(n.getRefId(), {
            label: n.getRefId(),
            width: n.width,
            height: n.height
          });
        });

    this.graph.getEdges()
        .forEach(e => {
          g.setEdge(e.parent.getRefId(), e.child.getRefId());
        });

    return g;
  }

  private layoutGraph(): void {
    const g = this.getDagreGraph();
    dagre.layout(g);

    this.graph.getNodes()
        .forEach(n => {
          const dagreNode = g.node(n.getRefId());

          if (dagreNode) {
            n.updatePoint(new VPoint(dagreNode.x, dagreNode.y));
          }
        });

    this.graph.getEdges()
        .forEach(e => {
          const dagreEdge = g.edge({v: e.parent.getRefId(), w: e.child.getRefId()});
          if (dagreEdge) {
            e.points = Array.from(dagreEdge.points)
                .map(point => {
                  const p = point as any;
                  return new VPoint(p.x, p.y);
                });
          }
        });
  }

  private drawEdges(): void {
    const graph = this.graph;
    const edges = d3.select(this.options.id)
        .selectAll('line')
        .data(graph.getEdges())
        .enter()
        .append('line')
        .each(function (d) {
          const path = graph.getPath(d.parent.id, d.child.id);
          d3.select(this).attr({
            'data-parent': d.parent.getRefId(),
            'data-child': d.child.getRefId(),
            x1: path.p1.x,
            y1: path.p1.y,
            x2: path.p2.x,
            y2: path.p2.y,
            style: 'stroke:rgb(255,0,0);stroke-width:2',
            class: 'edge-line',
            'marker-end': 'url(#arrow)'
          });
        });
  }

  private drawNodes(): void {
    const engine = this.engine;
    const graph = this.graph;
    const formatNodeName = this.formatNodeName;
    const formatPct = this.formatPct;
    const formatValue = this.formatValue;
    const leftPad = this.leftPad;
    const rightPad = this.rightPad;

    // set the node group
    const nodes = d3.select(this.options.id)
        .selectAll('g')
        .data(graph.getNodes())
        .enter()
        .append('g')
        .attr({
          id: function (d) {
            return d.getRefId();
          },
          transform: function (d) {
            return d.getTranslation();
          },
          class: 'node-group'
        })
        .on('mousedown', function (d) {
          d3.selectAll('g.node-group').sort((a, b) => {
            if (a.id !== d.id) {
              return -1;
            } else {
              return 1;
            }
          });
        });

    // draw the node rectangle
    nodes.append('rect')
        .attr({
          x: 0,
          y: 0,
          class: 'node-shape',
          style: 'stroke:#000000; fill:none;',
          width: function (d) {
            return d.width;
          },
          height: function (d) {
            return d.height;
          },
          'pointer-events': 'visible',
          'data-node': function (d) {
            return d.getRefId();
          }
        });

    // draw the node's name/label
    nodes.append('text')
        .attr({
          x: function (d) {
            return d.width / 2;
          },
          y: 15,
          fill: 'black',
          class: 'node-name',
          'font-family': 'monospace',
          'font-size': 15
        })
        .text(function (d) {
          return formatNodeName(d.label);
        })
        .style('text-anchor', 'middle');

    // draw the node's value labels
    nodes.each(function (d) {
      let y = 30;
      d.values.forEach(value => {
        d3.select(this)
            .append('text')
            .attr({
              x: 2,
              y: y,
              class: 'node-value',
              'font-family': 'monospace',
              'data-node': item => item.getRefId(),
              'data-value': value
            })
            .on('click', function (e) {
              const node = e as VNode;
              const h = this;
              const v = h.attributes['data-value'].value;

              engine.updateEvidence(node.id, v);
              graph.getNodes().forEach(nodeItem => nodeItem.probs = engine.getProbs(nodeItem.id));

              graph.getNodes().forEach(nodeItem => {
                nodeItem.values.forEach((valueItem, j) => {
                  // update belief bars
                  let selector = 'rect[data-node="' + nodeItem.getRefId() + '"][data-value="' + valueItem + '"]';
                  d3.select(selector).attr({width: nodeItem.probs[j] * 100});

                  // update probability texts
                  selector = 'text[data-node="' + nodeItem.getRefId() + '"][data-pvalue="' + valueItem + '"]';
                  d3.select(selector).text(formatPct(nodeItem.probs[j], leftPad));
                });
              });
            })
            .text(() => formatValue(value, leftPad));
        y += 15;
      });
    });

    // draw the node's percentage text
    nodes.each(function (d) {
      let y = 30;
      for (let i = 0; i < d.probs.length; i++) {
        d3.select(this)
            .append('text')
            .attr({
              x: 2 + d.width,
              y: y,
              'font-family': 'monospace',
              class: 'node-pct',
              'data-node': item => item.getRefId(),
              'data-pvalue': item => item.values[i]
            })
            .text(item => formatPct(item.probs[i], leftPad));
        y += 15;
      }
    });


    // draw the node's belief bars
    nodes.each(function (d) {
      let y = 20;
      d.probs.forEach((prob, index) => {
        d3.select(this)
            .append('rect')
            .attr({
              x: 50,
              y: y,
              width: prob * 100,
              height: 10,
              class: 'node-bar',
              'data-node': item => item.getRefId(),
              'data-value': item => item.values[index]
            });
        y += 15;
      });
    });

    // draw the node's interquartile lines
    nodes.each(function (d) {
      const y1 = 20;
      const y2 = d.height - 5;
      const width = d.width - 50;
      const xInc = width / 4.0;
      let x = 50 + xInc;

      for (let i = 0; i < 3; i++) {
        d3.select(this)
            .append('line')
            .attr({
              x1: x,
              y1: y1,
              x2: x,
              y2: y2,
              class: 'node-iqline',
              'stroke-dasharray': '5, 1',
              style: 'stroke:black; stroke-width:1px',
            });
        x += xInc;
      }
    });

    // set the dragging behavior
    const drag = d3.behavior.drag()
        .origin((d, i) => {
          const node = d as VNode;
          return node.point;
        })
        .on('dragstart', (d, i) => {
          const e = d3.event as d3.BaseEvent;
          e.sourceEvent.stopPropagation();
        })
        .on('drag', (d, i) => {
          const event = d3.event as any;
          const d3Event = d3.event as d3.BaseEvent;
          const mouseEvent = d3Event.sourceEvent as MouseEvent;
          const node = d as VNode;

          node.point.x = event.x;
          node.point.y = event.y;

          const id = 'g#' + node.getRefId();
          d3.select(id).attr({
            transform: node.getTranslation()
          });

          let arcs = 'line[data-parent=' + node.getRefId() + ']';
          d3.selectAll(arcs)
              .each(function (item) {
                const edge = item as VEdge;
                const path = graph.getPath(edge.parent.id, edge.child.id);
                d3.select(this).attr({
                  x1: path.p1.x,
                  y1: path.p1.y,
                  x2: path.p2.x,
                  y2: path.p2.y
                });
              });

          arcs = 'line[data-child=' + node.getRefId() + ']';
          d3.selectAll(arcs)
              .each(function (item) {
                const edge = item as VEdge;
                const path = graph.getPath(edge.parent.id, edge.child.id);
                d3.select(this).attr({
                  x1: path.p1.x,
                  y1: path.p1.y,
                  x2: path.p2.x,
                  y2: path.p2.y
                });
              });
        });
    nodes.call(drag);
  }

  private leftPad(pad: string, threshold: number, str: string): string {
    let ostr = str;
    if (ostr.length < threshold) {
      while (ostr.length < threshold) {
        ostr = pad + ostr;
      }
      return ostr;
    }
    return ostr;
  }

  private rightPad(pad: string, threshold: number, str: string): string {
    let ostr = str;
    if (ostr.length < threshold) {
      while (ostr.length < threshold) {
        ostr += pad;
      }
      return ostr;
    }
    return ostr;
  }

  private formatNodeName(v: string) {
    return v.length > MAX_NODE_NAME_LENGTH ? v.substr(0, MAX_NODE_NAME_LENGTH) : v;
  }

  private formatValue(v: string, padding: (pad: string, threshold: number, str: string) => string) {
    const value = v.length > MAX_VALUE_LENGTH ? v.substr(0, MAX_VALUE_LENGTH) : v;
    return padding(DEFAULT_PADDING, MAX_VALUE_LENGTH, value);
  }

  private formatPct(p: number, padding: (pad: string, threshold: number, str: string) => string): string {
    return padding(DEFAULT_PADDING, MAX_PCT_VALUE, (p * 100).toFixed(2));
  }
}
