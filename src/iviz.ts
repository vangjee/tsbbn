
class INode {
  public meta: Map<String, any>;

  constructor(public id: string) {
    this.meta = new Map<String, any>();
  }

  public addMeta(key: string, val: any): INode {
    this.meta.set(key, val);
    return this;
  }
}