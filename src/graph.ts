export class Variable {
  public values: string[];

  constructor(public id: number, public name: string, private _values: Set<string>) {
    this.values = [..._values];
  }
}

export class BGraph {
  protected nodes: Map<number, BNode>;
  protected edges: Map<number, Set<number>>;

  constructor() {
    this.nodes = new Map<number, BNode>();
    this.edges = new Map<number, Set<number>>();
  }

  public getNeighbors(id: number): Set<number> {
    const keys = Array.from(this.edges.keys());
    const neighbors1 = keys.filter(key => this.edges.get(key).has(id));
    const neighbors2 = this.edges.has(id) ? Array.from(this.edges.get(id).values()) : [];
    const neighbors = neighbors1.concat(neighbors2);
    return new Set(neighbors);
  }

  public getNode(id: number): BNode {
    return this.nodes.get(id);
  }

  public getNodes(): BNode[] {
    return Array.from(this.nodes.keys()).map(id => this.nodes.get(id));
  }

  public getEdges(): BEdge[] {
    const keys: number[] = Array.from(this.edges.keys());
    const stream = keys.map(lhsId => {
      const edges = Array.from(this.edges.get(lhsId))
          .map(rhsId => {
            let edgeType = EdgeType.UNDIRECTED;
            let leftId = lhsId;
            let rightId = rhsId;

            if (!this.edges.get(rhsId).has(lhsId)) {
              edgeType = EdgeType.DIRECTED;
            } else {
              leftId = Math.min(lhsId, rhsId);
              rightId = leftId === lhsId ? rhsId : lhsId;
            }

            return new BEdge(new BNode(leftId), new BNode(rightId), edgeType);
          });
      return edges;
    });

    return this.removeDuplicates(this.flatten(stream));
  }

  public addNode(node: BNode): BGraph {
    if (!this.nodes.has(node.id)) {
      this.nodes.set(node.id, node);
    }

    if (!this.edges.has(node.id)) {
      this.edges.set(node.id, new Set<number>());
    }
    return this;
  }

  public addEdge(edge: BEdge): BGraph {
    const lhs = edge.lhs;
    const rhs = edge.rhs;

    this.addNode(lhs);
    this.addNode(rhs);

    if (this.shouldAdd(edge)) {
      const left = lhs.id;
      const right = rhs.id;

      if (EdgeType.UNDIRECTED === edge.type) {
        this.edges.get(left).add(right);
        this.edges.get(right).add(left);
      } else {
        this.edges.get(left).add(right);
      }
    }

    return this;
  }

  public edgeExists(id1: number, id2: number): boolean {
    if (this.edges.has(id1) && this.edges.get(id1).has(id2)) {
      return true;
    }
    if (this.edges.has(id2) && this.edges.get(id2).has(id1)) {
      return true;
    }
    return false;
  }

  public removeNode(id: number) {
    this.nodes.delete(id);
    this.edges.delete(id);
    this.edges.forEach((v, k) => v.delete(id));
  }

  protected shouldAdd(edge: BEdge): boolean {
    const lhs = edge.lhs;
    const rhs = edge.rhs;

    if (lhs.id === rhs.id) {
      return false;
    }

    if (!this.edges.get(lhs.id).has(rhs.id)) {
      if (!this.edges.get(rhs.id).has(lhs.id)) {
        return true;
      }
    }

    return false;
  }

  private flatten(edges: BEdge[][]): BEdge[] {
    const list: BEdge[] = [];
    for (let i = 0; i < edges.length; i++) {
      for (let j = 0; j < edges[i].length; j++) {
        list.push(edges[i][j]);
      }
    }
    return list;
  }

  private removeDuplicates(edges: BEdge[]): BEdge[] {
    const map = new Map<string, BEdge>();
    const arr: BEdge[] = [];

    edges.forEach(e => {
      const key = e.toString();
      if (!map.has(key)) {
        map.set(key, e);
        arr.push(e);
      }
    });

    return arr;
  }
}

export class BNode {
  metadata: Map<String, any>;

  constructor(public id: number) {
    this.metadata = new Map<String, any>();
  }

  public addMetadata(key: String, val: any): BNode {
    this.metadata.set(key, val);
    return this;
  }
}

export class BbnNode extends BNode {
  probs: number[];
  potential: Potential;

  constructor(public variable: Variable, probs: number[] = []) {
    super(variable.id);
    this.probs = probs;
  }

  getWeight(): number {
    return this.variable.values.length;
  }

  public toString(): string {
    let s = this.variable.name + ' {';
    s += this.variable.values.join(',');
    s += '}';
    return s;
  }
}

export class Clique extends BNode {
  private _marked = false;

  constructor(public nodes: BbnNode[]) {
    super(IdUtil.hashOfArr(nodes.map(n => '' + n.id)));
  }

  public isMarked(): boolean {
    return this._marked;
  }

  public mark() {
    this._marked = true;
  }

  public unmark() {
    this._marked = false;
  }

  public nodesMinus(nodes: BbnNode[]): BbnNode[] {
    const ids = this.nodes.map(n => n.id);
    return nodes.filter(n => ids.indexOf(n.id) === -1);
  }

  public isSuperset(that: Clique): boolean {
    const s1 = this.nodes.map(n => n.id);
    const s2 = new Set(that.nodes.map(n => n.id));
    const s3 = new Set(s1.filter(id => s2.has(id)));

    if (s2.size === s3.size) {
      // console.log(this.toString() + ' is superset of ' + that.toString());
      return true;
    }
    // console.log(this.toString() + ' is NOT superset of ' + that.toString());
    return false;
  }

  public getWeight(): number {
    let weight = 1;
    this.nodes.forEach(n => {
      weight = weight * n.getWeight();
    });
    return weight;
  }

  public contains(id: number): boolean {
    const result = this.nodes.find(n => n.id === id);
    if (result) {
      return true;
    }
    return false;
  }

  public getSepSet(that: Clique): SepSet {
    return new SepSet(this, that);
  }

  public toString(): string {
    return '(' +
        this.nodes
            .map(n => n.variable.name)
            .sort((a, b) => CompareUtil.strCompare(a, b))
            .join('-') +
        ')';
  }
}

export class SepSet extends Clique {
  constructor(public left: Clique, public right: Clique) {
    super([]);

    const set2 = new Set(left.nodes.map(n => n.id));
    const set1 = new Set(right.nodes.filter(n => set2.has(n.id)).map(n => n.id));
    this.nodes = left.nodes.filter(n => set1.has(n.id));
    this.id = IdUtil.hashOfArr(this.nodes.map(n => '' + n.id));
  }

  public isEmpty(): boolean {
    return (this.nodes.length === 0) ? true : false;
  }

  public getCost(): number {
    return this.left.getWeight() + this.right.getWeight();
  }

  public getMass(): number {
    return this.nodes.length;
  }

  public toString(): string {
    return '[' +
        this.nodes
            .map(n => n.variable.name)
            .sort((a, b) => CompareUtil.strCompare(a, b))
            .join('-') +
        ']';
  }
}

export class BEdge {
  constructor(public lhs: BNode, public rhs: BNode, public type: EdgeType = EdgeType.UNDIRECTED) {

  }

  public toString(): string {
    const arrow = EdgeType.UNDIRECTED === this.type ? '--' : '->';
    let left = this.lhs.id;
    let right = this.rhs.id;

    if (EdgeType.UNDIRECTED === this.type) {
      left = Math.min(this.lhs.id, this.rhs.id);
      right = (this.lhs.id === left) ? this.rhs.id : this.lhs.id;
    }

    return left + arrow + right;
  }
}

export class JtEdge extends BEdge {
  constructor(public sepSet: SepSet) {
    super(sepSet.left, sepSet.right, EdgeType.UNDIRECTED);
  }

  public toString(): string {
    return this.sepSet.left.toString() + '--' +
        this.sepSet.toString() + '--' +
        this.sepSet.right.toString();
  }
}

export enum EdgeType {
  UNDIRECTED = 1, DIRECTED
}

export class Dag extends BGraph {

  constructor() {
    super();
  }

  public getParents(id: number): Set<number> {
    const keys = Array.from(this.edges.keys());
    const parents = keys.filter(key => this.edges.get(key).has(id));
    return new Set(parents);
  }

  public getChildren(id: number): Set<number> {
    return this.edges.get(id);
  }

  public edgeExists(id1: number, id2: number): boolean {
    if (this.edges.has(id1) && this.edges.get(id1).has(id2)) {
      return true;
    }
    return false;
  }

  protected shouldAdd(edge: BEdge): boolean {
    const parent = edge.lhs;
    const child = edge.rhs;

    if (parent.id === child.id) {
      return false;
    }

    if (!this.edges.get(parent.id).has(child.id)) {
      if (!this.edges.get(child.id).has(parent.id)) {
        if (!(new DagPathDetector(this, child.id, parent.id)).exists()) {
          return true;
        }
      }
    }

    return false;
  }
}

export class Bbn extends Dag {
  protected shouldAdd(edge: BEdge): boolean {
    if (!(edge.lhs instanceof BbnNode) || !(edge.rhs instanceof BbnNode)) {
      return false;
    }
    return super.shouldAdd(edge);
  }
}

class DagPathDetector {
  seen: Set<number>;
  graph: Dag;
  start: number;
  stop: number;

  constructor(graph: Dag, start: number, stop: number) {
    this.seen = new Set<number>();
    this.graph = graph;
    this.start = start;
    this.stop = stop;
  }

  public exists(): boolean {
    if (this.start === this.stop) {
      return true;
    }
    return this.find(this.start);
  }

  private find(id: number): boolean {
    const children = this.graph.getChildren(id);
    if (children.has(this.stop)) {
      return true;
    } else {
      this.seen.add(id);
      for (const child of children) {
        if (!this.seen.has(child) && this.find(child)) {
          return true;
        }
      }
    }
    return false;
  }
}

export class Pdag extends BGraph {
  public getParents(id: number): Set<number> {
    const parents = Array.from(this.edges.keys())
        .filter(key => this.edges.get(key).has(id) && !this.edges.get(id).has(key));
    return new Set(parents);
  }

  public getOutNodes(id: number): Set<number> {
    const parents = this.getParents(id);
    const subset = Array.from(this.getNeighbors(id)).filter(item => !parents.has(item));
    return new Set(subset);
  }

  protected shouldAdd(edge: BEdge): boolean {
    const parent = edge.lhs;
    const child = edge.rhs;

    if (parent.id === child.id) {
      return false;
    }

    if (!this.edges.get(parent.id).has(child.id)) {
      if (!this.edges.get(child.id).has(parent.id)) {
        if (!(new PdagPathDetector(this, child.id, parent.id)).exists()) {
          return true;
        }
      }
    }

    return false;
  }
}

class PdagPathDetector {
  seen: Set<number>;
  graph: Pdag;
  start: number;
  stop: number;

  constructor(graph: Pdag, start: number, stop: number) {
    this.seen = new Set<number>();
    this.graph = graph;
    this.start = start;
    this.stop = stop;
  }

  public exists(): boolean {
    if (this.start === this.stop) {
      return true;
    }
    return this.find(this.start);
  }

  private find(id: number): boolean {
    const outNodes = this.graph.getOutNodes(id);
    if (outNodes.has(this.stop)) {
      return true;
    } else {
      this.seen.add(id);
      for (const outNode of outNodes) {
        if (!this.seen.has(outNode)) {
          if (this.find(outNode)) {
            return true;
          }
        }
      }
    }

    return false;
  }
}

export class Ug extends BGraph {
  constructor() {
    super();
  }
}

export class JoinTree extends Ug {
  listener: JoinTreeListener;
  potentials: Map<number, Potential>;
  evidences: Map<number, Map<string, Potential>>;

  constructor() {
    super();
    this.potentials = new Map<number, Potential>();
    this.evidences = new Map<number, Map<string, Potential>>();
  }

  public getBbnPotential(node: BbnNode): Potential {
    const clique = node.metadata.get('parent.clique') as Clique;
    return PotentialUtil.normalize(PotentialUtil.marginalizeFor(this, clique, [node]));
  }

  public unmarkCliques() {
    this.getCliques().forEach(clique => clique.unmark());
  }

  public getBbnNodes(): BbnNode[] {
    const nodes = new Map<number, BbnNode>();

    this.getCliques().forEach(clique => {
      clique.nodes.forEach(node => nodes.set(node.id, node));
    });

    const bbnNodes: BbnNode[] = [];
    nodes.forEach((v, k) => bbnNodes.push(v));
    return bbnNodes;
  }

  public getBbnNode(id: number): BbnNode {
    const bbnNodes = this.getBbnNodes();
    for (let i = 0; i < bbnNodes.length; i++) {
      if (id === bbnNodes[i].id) {
        return bbnNodes[i];
      }
    }
    return new BbnNode(new Variable(-1, '_dummy_', new Set()));
  }

  public getBbnNodeByName(name: string): BbnNode {
    return this.getBbnNodes().filter(n => n.variable.name === name)[0];
  }

  public findCliquesWithNodeAndParents(id: number): Clique[] {
    return this.getCliques()
        .filter(clique => {
          if (!(clique.contains(id))) {
            return false;
          }

          const bbnNode = this.getBbnNode(id);
          if (bbnNode.metadata.has('parents')) {
            const parents: BNode[] = bbnNode.metadata.get('parents') as BNode[];
            for (let i = 0; i < parents.length; i++) {
              const parent = parents[i];
              if (!(clique.contains(parent.id))) {
                return false;
              }
            }
          }

          return true;
        });
  }

  public addPotential(clique: Clique, potential: Potential): JoinTree {
    this.potentials.set(clique.id, potential);
    return this;
  }

  public getCliques(): Clique[] {
    return this.getNodes()
        .filter(n => !(n instanceof SepSet))
        .map(n => n as Clique);
  }

  public getSepSets(): SepSet[] {
    return this.getNodes()
        .filter(n => (n instanceof SepSet))
        .map(n => n as SepSet);
  }

  public addEdge(edge: BEdge): BGraph {
    if (!(edge instanceof JtEdge)) {
      return this;
    }

    const jtEdge = edge as JtEdge;
    const sepSet = jtEdge.sepSet;
    const lhs = jtEdge.lhs;
    const rhs = jtEdge.rhs;

    if (this.shouldAdd(edge)) {
      this.addNode(sepSet);
      this.addNode(lhs);
      this.addNode(rhs);

      this.edges.get(lhs.id).add(sepSet.id);
      this.edges.get(sepSet.id).add(lhs.id);

      this.edges.get(rhs.id).add(sepSet.id);
      this.edges.get(sepSet.id).add(rhs.id);
    }

    return this;
  }

  public setListener(listener: JoinTreeListener) {
    this.listener = listener;
  }

  public getEvidence(node: BbnNode, value: string): Potential {
    if (!this.evidences.has(node.id)) {
      this.evidences.set(node.id, new Map<string, Potential>());
    }

    if (!this.evidences.get(node.id).has(value)) {
      const potentialEntry = new PotentialEntry();
      potentialEntry.add(node.id, value);
      potentialEntry.value = 1.0;

      const potential = new Potential();
      potential.addEntry(potentialEntry);

      this.evidences.get(node.id).set(value, potential);
    }

    return this.evidences.get(node.id).get(value);
  }

  public unobserve(nodes: BbnNode[]): JoinTree {
    const evidences = nodes.map(n => this.getUnobservedEvidence(n));
    this.updateEvidences(evidences);
    return this;
  }

  public unobserveAll(): JoinTree {
    this.unobserve(this.getBbnNodes());
    return this;
  }

  public updateEvidences(evidences: Evidence[]): JoinTree {
    evidences.forEach(e => e.validate());
    const change = this.getChangeType(evidences);
    evidences.forEach(evidence => {
      const node = evidence.node;
      const potentials = this.evidences.get(node.id);

      evidence.values.forEach((v, k) => {
        const potential = potentials.get(k);
        potential.entries[0].value = v;
      });
    });
    this.notifyListener(change);
    return this;
  }

  public setObservation(evidence: Evidence): JoinTree {
    // only deal with observation types in this method
    if (EvidenceType.OBSERVATION !== evidence.type) {
      // console.log('evidence not observation type returning');
      return this;
    }

    // check to see if previous evidence was also observation
    // evidence that is observation type always has only one value that is 1
    const potentials = this.evidences.get(evidence.node.id);
    const pvalues = Array.from(potentials.keys())
        .filter(v => {
          const potential = potentials.get(v);
          const entry = potential.entries[0];
          const p = entry.value;
          if (p === 1.0) {
            return true;
          }
          return false;
        });
    const cvalues = Array.from(evidence.values.keys())
        .filter(v => 1.0 === evidence.values.get(v));

    if (1 === pvalues.length) {
      // previous evidence was of type observation
      // both current and previous evidences are observation type
      // console.log('both previous and current evidences are of type observation');

      const lastValue = pvalues[0];
      const currValue = cvalues[0];
      if (lastValue === currValue) {
        // if the last value is equal to the current value, unobserve it
        // console.log(lastValue + ' equals ' + currValue + ' so will unobserve');
        this.unobserve([evidence.node]);
      } else {
        // console.log(lastValue + ' NOT equals ' + currValue + ' so will update evidence');
        this.updateEvidences([evidence]);
      }
    } else {
      // console.log('number previous values === 1 was ' + pvalues.length + ' so will update evidence');
      this.updateEvidences([evidence]);
    }

    return this;
  }

  protected shouldAdd(edge: BEdge): boolean {
    const jtEdge = edge as JtEdge;
    const sepSet = jtEdge.sepSet;
    const lhs = jtEdge.lhs as Clique;
    const rhs = jtEdge.rhs as Clique;

    if (lhs.id === rhs.id) {
      // console.log('false: ' + jtEdge.toString());
      return false;
    }

    if (!(new JoinTreePathDetector(this, lhs.id, rhs.id)).exists()) {
      // console.log('true: ' +  jtEdge.toString());
      return true;
    }

    // console.log('false: ' + jtEdge.toString());
    return false;
  }

  private getChangeType(evidences: Evidence[]): ChangeType {
    const changes = evidences.map(evidence => {
      const node = evidence.node;
      const potentials = this.evidences.get(node.id);
      const change = evidence.compare(potentials);
      return change;
    });
    let count = changes.filter(c => ChangeType.RETRACTION === c).length;
    if (count > 0) {
      return ChangeType.RETRACTION;
    }

    count = changes.filter(c => ChangeType.UPDATE === c).length;
    if (count > 0) {
      return ChangeType.UPDATE;
    }

    return ChangeType.NONE;
  }

  private getUnobservedEvidence(node: BbnNode): Evidence {
    const evidence = new Evidence(node, EvidenceType.UNOBSERVE);
    node.variable.values.forEach(v => evidence.addValue(v, 1.0));
    return evidence;
  }

  private notifyListener(change: ChangeType) {
    if (this.listener) {
      if (ChangeType.RETRACTION === change) {
        this.listener.evidenceRetracted(this);
      } else if (ChangeType.UPDATE === change) {
        this.listener.evidenceUpdated(this);
      }
    }
  }
}

class JoinTreePathDetector {
  seen: Set<number>;
  graph: JoinTree;
  start: number;
  stop: number;

  constructor(graph: JoinTree, start: number, stop: number) {
    this.seen = new Set<number>();
    this.graph = graph;
    this.start = start;
    this.stop = stop;
  }

  public exists(): boolean {
    if (this.start === this.stop) {
      return true;
    }
    return this.find(this.start);
  }

  private find(id: number): boolean {
    const neighbors = this.graph.getNeighbors(id);
    if (neighbors.has(this.stop)) {
      return true;
    } else {
      this.seen.add(id);
      for (const neighbor of neighbors) {
        if (!this.seen.has(neighbor)) {
          if (this.find(neighbor)) {
            return true;
          }
        }
      }
    }

    return false;
  }
}

export interface JoinTreeListener {
  evidenceRetracted(jointree: JoinTree): void;

  evidenceUpdated(jointree: JoinTree): void;
}

export enum EvidenceType {
  VIRTUAL = 1, FINDING, OBSERVATION, UNOBSERVE
}

export enum ChangeType {
  NONE = 1, UPDATE, RETRACTION
}

export class EvidenceBuilder {
  values = new Map<string, number>();
  node: BbnNode;
  type = EvidenceType.OBSERVATION;

  public withNode(node: BbnNode): EvidenceBuilder {
    this.node = node;
    return this;
  }

  public withType(type: EvidenceType): EvidenceBuilder {
    this.type = type;
    return this;
  }

  public withEvidence(val: string, likelihood: number): EvidenceBuilder {
    this.values.set(val, likelihood);
    return this;
  }

  public build(): Evidence {
    const evidence = new Evidence(this.node, this.type);
    this.values.forEach((v, k) => evidence.values.set(k, v));
    return evidence;
  }
}

export class Evidence {
  values: Map<string, number>;

  constructor(public node: BbnNode, public type: EvidenceType) {
    this.values = new Map<string, number>();
  }

  public addValue(value: string, likelihood: number): Evidence {
    this.values.set(value, likelihood);
    return this;
  }

  public compare(potentials: Map<string, Potential>): ChangeType {
    const that = this.convert(potentials);

    const unobserveThat = this.isUnobserved(that);
    const unobserveThis = this.isUnobserved(this.values);

    if (unobserveThat && unobserveThis) {
      return ChangeType.NONE;
    }

    const observeThat = this.isObserved(that);
    const observeThis = this.isObserved(this.values);

    if (observeThat && observeThis) {
      const s1 = this.getObservedValue(that);
      const s2 = this.getObservedValue(this.values);

      if (s1 === s2) {
        return ChangeType.NONE;
      } else {
        return ChangeType.RETRACTION;
      }
    }

    return ChangeType.RETRACTION;
  }

  public validate() {
    this.node.variable.values.forEach(value => {
      if (!this.values.has(value)) {
        this.values.set(value, 0.0);
      }
    });

    if (EvidenceType.VIRTUAL === this.type) {
      const sum = this.node.variable.values
          .map(value => this.values.get(value))
          .reduce((a, b) => a + b);
      this.node.variable.values.forEach(value => {
        const d = this.values.get(value) / sum;
        this.values.set(value, d);
      });
    } else if (EvidenceType.FINDING === this.type) {
      this.node.variable.values.forEach(value => {
        const d = this.values.get(value) > 0.0 ? 1.0 : 0.0;
        this.values.set(value, d);
      });

      const count = this.node.variable.values
          .map(value => this.values.get(value))
          .reduce((a, b) => a + b);

      if (0 === count) {
        this.node.variable.values.forEach(value => this.values.set(value, 1.0));
      }
    } else if (EvidenceType.OBSERVATION === this.type) {
      const keys = Array.from(this.values.keys())
          .sort((a, b) => -1 * CompareUtil.intCompare(this.values.get(a), this.values.get(b)));
      const key = keys[0];

      this.node.variable.values.forEach(value => {
        if (key === value) {
          this.values.set(value, 1.0);
        } else {
          this.values.set(value, 0.0);
        }
      });
    } else if (EvidenceType.UNOBSERVE === this.type) {
      this.node.variable.values.forEach(value => this.values.set(value, 1.0));
    }
  }

  private convert(map: Map<string, Potential>): Map<string, number> {
    const m = new Map<string, number>();
    map.forEach((v, k) => {
      const likelihood = v.entries[0].value;
      m.set(k, likelihood);
    });
    return m;
  }

  private isUnobserved(values: Map<string, number>): boolean {
    let counts = 0;
    values.forEach((v, k) => counts += v);
    return (counts === values.size);
  }

  private isObserved(values: Map<string, number>): boolean {
    let countOne = 0;
    let countZero = 0;
    values.forEach((v, k) => {
      if (1.0 === v) {
        countOne++;
      } else if (0.0 === v) {
        countZero++;
      }
    });

    return (1 === countOne && values.size - 1 === countZero);
  }

  private getObservedValue(values: Map<string, number>): string {
    const strs = Array.from(values.keys())
        .filter(k => (1.0 === values.get(k)));
    return strs[0];
  }
}

export class Potential {
  public entries: PotentialEntry[];

  constructor() {
    this.entries = [];
  }

  public addEntry(entry: PotentialEntry) {
    this.entries.push(entry);
  }

  public getMatchingEntries(entry: PotentialEntry): PotentialEntry[] {
    return this.entries.filter(e => e.matches(entry));
  }

  public toString(): string {
    let s = '';
    this.entries.forEach(entry => s += entry.toString() + '\n');
    return s.substr(0, s.length - 1);
  }
}

export class PotentialEntry {
  public entries: Map<number, string>;
  public value: number;

  constructor() {
    this.entries = new Map<number, string>();
    this.value = 1.0;
  }

  public add(id: number, value: string): PotentialEntry {
    if (!this.entries.has(id)) {
      this.entries.set(id, value);
    }
    return this;
  }

  public matches(that: PotentialEntry): boolean {
    for (const key of that.entries.keys()) {
      if (!this.entries.has(key) || !(this.entries.get(key) === that.entries.get(key))) {
        return false;
      }
    }
    return true;
  }

  public toString(): string {
    let s = '';
    this.entries.forEach((v, k) => {
      s += k + '=' + v + ',';
    });
    s += this.value;
    return s;
  }

  public duplicate(): PotentialEntry {
    const entry = new PotentialEntry();
    this.entries.forEach((v, k) => entry.add(k, v));
    entry.value = this.value;
    return entry;
  }
}

export class IdUtil {
  static hashOfStr(s: string): number {
    let hash = 0;
    if (s.length === 0) {
      return hash;
    }

    for (let i = 0, len = s.length; i < len; i++) {
      const chr = s.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }

  static hashOfArr(arr: string[]): number {
    return this.hashOfStr(arr.sort().join('-'));
  }
}

export class PotentialUtil {
  public static passSingleMessage(jointree: JoinTree, x: Clique, s: SepSet, y: Clique) {
    // console.log('passing message ' + x.toString() + ' -- ' + s.toString() + ' -- ' + y.toString());
    const oldSepSetPotential = jointree.potentials.get(s.id);
    const yPotential = jointree.potentials.get(y.id);

    const newSepSetPotential = this.marginalizeFor(jointree, x, s.nodes);
    jointree.addPotential(s, newSepSetPotential);

    this.multiply(yPotential, this.divide(newSepSetPotential, oldSepSetPotential));
  }

  public static marginalizeFor(joinTree: JoinTree, clique: Clique, nodes: BbnNode[]): Potential {
    const potential = this.getPotentialFromNodes(nodes);
    const cliquePotential = joinTree.potentials.get(clique.id);

    potential.entries.forEach(entry => {
      const matchedEntries = cliquePotential.getMatchingEntries(entry);
      let t = 0.0;
      matchedEntries.forEach(matchedEntry => t += matchedEntry.value);
      entry.value = t;
    });

    return potential;
  }

  public static normalize(potential: Potential): Potential {
    let sum = 0.0;
    potential.entries.forEach(entry => {
      sum += entry.value;
    });

    potential.entries.forEach(entry => {
      const d = entry.value / sum;
      entry.value = d;
    });

    return potential;
  }

  private static isZero(d: number): boolean {
    return (0 === d);
  }

  public static divide(numerator: Potential, denominator: Potential): Potential {
    const potential = new Potential();
    numerator.entries.forEach(entry => {
      const entries = denominator.entries;
      if (entries.length > 0) {
        const e = entries[0];
        const d = (PotentialUtil.isZero(entry.value) || PotentialUtil.isZero(e.value)) ? 0.0 : entry.value / e.value;
        const newEntry = entry.duplicate();
        newEntry.value = d;
        potential.addEntry(newEntry);
      }
    });
    return potential;
  }

  public static multiply(bigger: Potential, smaller: Potential) {
    smaller.entries.forEach(entry => {
      const entries = bigger.getMatchingEntries(entry);
      entries.forEach(e => {
        const d = e.value * entry.value;
        e.value = d;
      });
    });
  }

  public static getPotential(node: BbnNode, parents: BbnNode[]) {
    const potential = this.getPotentialFromNodes(this.merge(node, parents));
    const total = potential.entries.length;
    for (let i = 0; i < total; i++) {
      const prob = node.probs[i];
      potential.entries[i].value = prob;
    }
    return potential;
  }

  public static getPotentialFromNodes(nodes: BbnNode[]): Potential {
    const valueLists = nodes.map(n => n.variable.values);
    const cartesian = this.getCartesianProduct(valueLists);
    const potential = new Potential();
    cartesian.forEach(values => {
      const entry = new PotentialEntry();
      for (let i = 0; i < nodes.length; i++) {
        const value = values[i];
        const id = nodes[i].id;
        entry.add(id, value);
      }
      potential.addEntry(entry);
    });
    return potential;
  }

  private static getCartesianProduct(lists: string[][]): string[][] {
    const results: string[][] = [];
    if (lists.length === 0) {
      return results;
    } else {
      const first = lists[0];
      const tail = lists.slice(0).splice(1);
      const remaining = this.getCartesianProduct(tail);
      first.forEach(condition => {
        if (remaining.length > 0) {
          remaining.forEach(rlist => {
            const result: string[] = [];
            result.push(condition);
            rlist.forEach(r => result.push(r));
            results.push(result);
          });
        } else {
          const result: string[] = [];
          result.push(condition);
          results.push(result);
        }
      });
    }
    return results;
  }

  private static merge(node: BbnNode, parents: BbnNode[]): BbnNode[] {
    const nodes: BbnNode[] = [];
    parents.forEach(n => nodes.push(n));
    nodes.push(node);

    return nodes;
  }
}

export class BbnUtil {
  public static getHuangGraph(): Bbn {
    const a = new BbnNode(new Variable(0, 'a', new Set<string>(['on', 'off'])), [0.5, 0.5]);
    const b = new BbnNode(new Variable(1, 'b', new Set<string>(['on', 'off'])), [0.5, 0.5, 0.4, 0.6]);
    const c = new BbnNode(new Variable(2, 'c', new Set<string>(['on', 'off'])), [0.7, 0.3, 0.2, 0.8]);
    const d = new BbnNode(new Variable(3, 'd', new Set<string>(['on', 'off'])), [0.9, 0.1, 0.5, 0.5]);
    const e = new BbnNode(new Variable(4, 'e', new Set<string>(['on', 'off'])), [0.3, 0.7, 0.6, 0.4]);
    const f = new BbnNode(new Variable(5, 'f', new Set<string>(['on', 'off'])), [0.01, 0.99, 0.01, 0.99, 0.01, 0.99, 0.99, 0.01]);
    const g = new BbnNode(new Variable(6, 'g', new Set<string>(['on', 'off'])), [0.8, 0.2, 0.1, 0.9]);
    const h = new BbnNode(new Variable(7, 'h', new Set<string>(['on', 'off'])), [0.05, 0.95, 0.95, 0.05, 0.95, 0.05, 0.95, 0.05]);

    const bbn = new Bbn()
        .addNode(a)
        .addNode(b)
        .addNode(c)
        .addNode(d)
        .addNode(e)
        .addNode(f)
        .addNode(g)
        .addNode(h)
        .addEdge(new BEdge(a, b, EdgeType.DIRECTED))
        .addEdge(new BEdge(a, c, EdgeType.DIRECTED))
        .addEdge(new BEdge(b, d, EdgeType.DIRECTED))
        .addEdge(new BEdge(c, e, EdgeType.DIRECTED))
        .addEdge(new BEdge(d, f, EdgeType.DIRECTED))
        .addEdge(new BEdge(e, f, EdgeType.DIRECTED))
        .addEdge(new BEdge(c, g, EdgeType.DIRECTED))
        .addEdge(new BEdge(e, h, EdgeType.DIRECTED))
        .addEdge(new BEdge(g, h, EdgeType.DIRECTED)) as Bbn;
    return bbn;
  }
}

export class CompareUtil {
  public static intCompare(x: number, y: number): number {
    return (x < y) ? -1 : ((x === y) ? 0 : 1);
  }

  public static strCompare(x: string, y: string): number {
    return x.localeCompare(y);
  }
}
