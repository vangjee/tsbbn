import {assert, expect} from 'chai';
import {
  Bbn,
  BbnNode,
  BEdge,
  BNode,
  Clique,
  Dag,
  EdgeType,
  IdUtil,
  JoinTree,
  JtEdge,
  Pdag,
  Ug,
  Variable
} from '../src/graph';

describe('testing bbn', () => {
  it('should be able to create bbn', () => {
    let a = new BbnNode(new Variable(0, 'a', new Set(['t', 'f'])), [0.5, 0.5]);
    let b = new BbnNode(new Variable(1, 'b', new Set(['t', 'f'])), [0.5, 0.5, 0.4, 0.6]);
    let c = new BbnNode(new Variable(2, 'c', new Set(['t', 'f'])), [0.7, 0.3, 0.2, 0.8]);
    
    let bbn = new Bbn()
      .addNode(a)
      .addNode(b)
      .addNode(c)
      .addEdge(new BEdge(a, b, EdgeType.DIRECTED))
      .addEdge(new BEdge(b, c, EdgeType.DIRECTED));
    
    let nodes = bbn.getNodes();
    let edges = bbn.getEdges();
    
    expect(nodes.length).to.be.equal(3);
    expect(edges.length).to.be.equal(2);
  });
  
  it('should create the huang example bbn', () => {
    let a = new BbnNode(new Variable(0, 'a', new Set(['on', 'off'])), [0.5, 0.5]);
    let b = new BbnNode(new Variable(1, 'b', new Set(['on', 'off'])), [0.5, 0.5, 0.4, 0.6]);
    let c = new BbnNode(new Variable(3, 'c', new Set(['on', 'off'])), [0.7, 0.3, 0.2, 0.8]);
    let d = new BbnNode(new Variable(4, 'd', new Set(['on', 'off'])), [0.9, 0.1, 0.5, 0.5]);
    let e = new BbnNode(new Variable(5, 'e', new Set(['on', 'off'])), [0.3, 0.7, 0.6, 0.4]);
    let f = new BbnNode(new Variable(6, 'f', new Set(['on', 'off'])), [0.01, 0.99, 0.01, 0.99, 0.01, 0.99, 0.99, 0.01]);
    let g = new BbnNode(new Variable(7, 'g', new Set(['on', 'off'])), [0.8, 0.2, 0.1, 0.9]);
    let h = new BbnNode(new Variable(8, 'h', new Set(['on', 'off'])), [0.05, 0.95, 0.95, 0.05, 0.95, 0.05, 0.95, 0.05]);
    
    let bbn = new Bbn()
      .addNode(a)
      .addNode(b)
      .addNode(c)
      .addNode(d)
      .addNode(e)
      .addNode(f)
      .addNode(g)
      .addNode(h)
      .addEdge(new BEdge(a, b, EdgeType.DIRECTED))
      .addEdge(new BEdge(a, c, EdgeType.DIRECTED))
      .addEdge(new BEdge(b, d, EdgeType.DIRECTED))
      .addEdge(new BEdge(c, e, EdgeType.DIRECTED))
      .addEdge(new BEdge(d, f, EdgeType.DIRECTED))
      .addEdge(new BEdge(e, f, EdgeType.DIRECTED))
      .addEdge(new BEdge(c, g, EdgeType.DIRECTED))
      .addEdge(new BEdge(e, h, EdgeType.DIRECTED))
      .addEdge(new BEdge(g, h, EdgeType.DIRECTED));
    
    let nodes = bbn.getNodes();
    let edges = bbn.getEdges();
    
    expect(nodes.length).to.be.equal(8);
    expect(edges.length).to.be.equal(9);
  });
});

describe('testing dag', () => {
  it('should get the children correctly', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.DIRECTED);
    let g = new Dag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2);
    
    let ch0 = g.getChildren(0);
    let ch1 = g.getChildren(1);
    let ch2 = g.getChildren(2);
    
    expect(ch0.size).to.equal(1);
    expect(ch1.size).to.equal(1);
    expect(ch2.size).to.equal(0);
    
    expect(ch0.has(1)).to.be.true;
    expect(ch1.has(2)).to.be.true;
  });
  
  it('should get the parents correctly', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.DIRECTED);
    let g = new Dag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2);
    
    let pa0 = g.getParents(0);
    let pa1 = g.getParents(1);
    let pa2 = g.getParents(2);
    
    expect(pa0.size).to.equal(0);
    expect(pa1.size).to.equal(1);
    expect(pa2.size).to.equal(1);
    
    expect(pa1.has(0)).to.be.true;
    expect(pa2.has(1)).to.be.true;
  });
  
  it('should get the neighbors correctly', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.DIRECTED);
    let g = new Dag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2);
    
    let ne0 = g.getNeighbors(0);
    let ne1 = g.getNeighbors(1);
    let ne2 = g.getNeighbors(2);
    
    expect(ne0.size).to.equal(1);
    expect(ne1.size).to.equal(2);
    expect(ne2.size).to.equal(1);
    
    expect(ne0.has(1)).to.be.true;
    expect(ne1.has(0)).to.be.true;
    expect(ne1.has(2)).to.be.true;
    expect(ne2.has(1)).to.be.true;
  });
  
  it('should add nodes and edges correctly', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.DIRECTED);
    let g = new Dag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2);
    
    let nodes = g.getNodes();
    let edges = g.getEdges();
    
    expect(nodes.length).to.equal(3);
    
    expect(nodes.find(e => e.id === 0).id).to.equal(0);
    expect(nodes.find(e => e.id === 1).id).to.equal(1);
    expect(nodes.find(e => e.id === 2).id).to.equal(2);
    
    expect(edges.length).to.equal(2);
  });
  
  it('should not add cycles', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.DIRECTED);
    let e3 = new BEdge(n2, n0, EdgeType.DIRECTED);
    let e4 = new BEdge(n0, n0, EdgeType.DIRECTED);
    let g = new Dag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2)
      .addEdge(e3)
      .addEdge(e4);
    
    let nodes = g.getNodes();
    let edges = g.getEdges();
    
    expect(nodes.length).to.equal(3);
    
    expect(nodes.find(e => e.id === 0).id).to.equal(0);
    expect(nodes.find(e => e.id === 1).id).to.equal(1);
    expect(nodes.find(e => e.id === 2).id).to.equal(2);
    
    expect(edges.length).to.equal(2);
  });
});

describe('testing pdag', () => {
  
  it('should get the parents correctly', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.DIRECTED);
    let g = new Pdag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2);
    
    let pa0 = g.getParents(0);
    let pa1 = g.getParents(1);
    let pa2 = g.getParents(2);
    
    expect(pa0.size).to.equal(0);
    expect(pa1.size).to.equal(1);
    expect(pa2.size).to.equal(1);
    
    expect(pa1.has(0)).to.be.true;
    expect(pa2.has(1)).to.be.true;
  });
  
  it('should get the parents correctly with mixed edge types', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.UNDIRECTED);
    let g = new Pdag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2);
    
    let pa0 = g.getParents(0);
    let pa1 = g.getParents(1);
    let pa2 = g.getParents(2);
    
    expect(pa0.size).to.equal(0);
    expect(pa1.size).to.equal(1);
    expect(pa2.size).to.equal(0);
    
    expect(pa1.has(0)).to.be.true;
  });
  
  it('should get the neighbors correctly', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.DIRECTED);
    let g = new Pdag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2);
    
    let ne0 = g.getNeighbors(0);
    let ne1 = g.getNeighbors(1);
    let ne2 = g.getNeighbors(2);
    
    expect(ne0.size).to.equal(1);
    expect(ne1.size).to.equal(2);
    expect(ne2.size).to.equal(1);
    
    expect(ne0.has(1)).to.be.true;
    expect(ne1.has(0)).to.be.true;
    expect(ne1.has(2)).to.be.true;
    expect(ne2.has(1)).to.be.true;
  });
  
  it('should get the neighbors correctly with mixed edge types', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.UNDIRECTED);
    let g = new Pdag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2);
    
    let ne0 = g.getNeighbors(0);
    let ne1 = g.getNeighbors(1);
    let ne2 = g.getNeighbors(2);
    
    expect(ne0.size).to.equal(1);
    expect(ne1.size).to.equal(2);
    expect(ne2.size).to.equal(1);
    
    expect(ne0.has(1)).to.be.true;
    expect(ne1.has(0)).to.be.true;
    expect(ne1.has(2)).to.be.true;
    expect(ne2.has(1)).to.be.true;
  });
  
  it('should add nodes and edges correctly', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.DIRECTED);
    let g = new Pdag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2);
    
    let nodes = g.getNodes();
    let edges = g.getEdges();
    
    expect(nodes.length).to.equal(3);
    
    expect(nodes.find(e => e.id === 0).id).to.equal(0);
    expect(nodes.find(e => e.id === 1).id).to.equal(1);
    expect(nodes.find(e => e.id === 2).id).to.equal(2);
    
    expect(edges.length).to.equal(2);
  });
  
  it('should not add cycles', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.DIRECTED);
    let e3 = new BEdge(n2, n0, EdgeType.DIRECTED);
    let e4 = new BEdge(n0, n0, EdgeType.DIRECTED);
    let g = new Pdag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2)
      .addEdge(e3)
      .addEdge(e4);
    
    let nodes = g.getNodes();
    let edges = g.getEdges();
    
    expect(nodes.length).to.equal(3);
    
    expect(nodes.find(e => e.id === 0).id).to.equal(0);
    expect(nodes.find(e => e.id === 1).id).to.equal(1);
    expect(nodes.find(e => e.id === 2).id).to.equal(2);
    
    expect(edges.length).to.equal(2);
  });
  
  it('should not add cycles with mixed edge types', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.DIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.DIRECTED);
    let e3 = new BEdge(n2, n0, EdgeType.DIRECTED);
    let e4 = new BEdge(n0, n0, EdgeType.DIRECTED);
    let e5 = new BEdge(n2, n0, EdgeType.UNDIRECTED);
    let g = new Pdag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2)
      .addEdge(e3)
      .addEdge(e4)
      .addEdge(e5);
    
    let nodes = g.getNodes();
    let edges = g.getEdges();
    
    expect(nodes.length).to.equal(3);
    
    expect(nodes.find(e => e.id === 0).id).to.equal(0);
    expect(nodes.find(e => e.id === 1).id).to.equal(1);
    expect(nodes.find(e => e.id === 2).id).to.equal(2);
    
    expect(edges.length).to.equal(2);
  });
  
  it('should not add cycles with undirected edge types', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1, EdgeType.UNDIRECTED);
    let e2 = new BEdge(n1, n2, EdgeType.UNDIRECTED);
    let e3 = new BEdge(n2, n0, EdgeType.UNDIRECTED);
    let e4 = new BEdge(n0, n0, EdgeType.UNDIRECTED);
    let g = new Pdag();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2)
      .addEdge(e3)
      .addEdge(e4);
    
    let nodes = g.getNodes();
    let edges = g.getEdges();
    
    expect(nodes.length).to.equal(3);
    
    expect(nodes.find(e => e.id === 0).id).to.equal(0);
    expect(nodes.find(e => e.id === 1).id).to.equal(1);
    expect(nodes.find(e => e.id === 2).id).to.equal(2);
    
    expect(edges.length).to.equal(2);
  });
});

describe('testing undirected graph', () => {
  
  it('should get the neighbors correctly', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1);
    let e2 = new BEdge(n1, n2);
    let g = new Ug();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2);
    
    let ne0 = g.getNeighbors(0);
    let ne1 = g.getNeighbors(1);
    let ne2 = g.getNeighbors(2);
    
    expect(ne0.size).to.equal(1);
    expect(ne1.size).to.equal(2);
    expect(ne2.size).to.equal(1);
    
    expect(ne0.has(1)).to.be.true;
    expect(ne1.has(0)).to.be.true;
    expect(ne1.has(2)).to.be.true;
    expect(ne2.has(1)).to.be.true;
  });
  
  it('should add nodes and edges correctly', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1);
    let e2 = new BEdge(n1, n2);
    let g = new Ug();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2);
    
    let nodes = g.getNodes();
    let edges = g.getEdges();
    
    expect(nodes.length).to.equal(3);
    
    expect(nodes.find(e => e.id === 0).id).to.equal(0);
    expect(nodes.find(e => e.id === 1).id).to.equal(1);
    expect(nodes.find(e => e.id === 2).id).to.equal(2);
    
    expect(edges.length).to.equal(2);
  });
  
  it('should allow cycles', () => {
    let n0 = new BNode(0);
    let n1 = new BNode(1);
    let n2 = new BNode(2);
    let e1 = new BEdge(n0, n1);
    let e2 = new BEdge(n1, n2);
    let e3 = new BEdge(n2, n0);
    let e4 = new BEdge(n0, n0);
    let g = new Ug();
    
    g.addNode(n0)
      .addNode(n1)
      .addNode(n2)
      .addEdge(e1)
      .addEdge(e2)
      .addEdge(e3)
      .addEdge(e4);
    
    let nodes = g.getNodes();
    let edges = g.getEdges();
    
    expect(nodes.length).to.equal(3);
    
    expect(nodes.find(e => e.id === 0).id).to.equal(0);
    expect(nodes.find(e => e.id === 1).id).to.equal(1);
    expect(nodes.find(e => e.id === 2).id).to.equal(2);
    
    expect(edges.length).to.equal(3);
  });
});

describe('test jointree', () => {
  it('should add nodes and arcs correctly', () => {
    let n0 = new BbnNode(new Variable(0, 'n0', new Set(['t', 'f'])));
    let n1 = new BbnNode(new Variable(1, 'n1', new Set(['t', 'f'])));
    let n2 = new BbnNode(new Variable(2, 'n2', new Set(['t', 'f'])));
    
    let clique0 = new Clique([n0, n1]);
    let clique1 = new Clique([n1, n2]);
    let sepSet0 = clique0.getSepSet(clique1);
    
    let e0 = new JtEdge(sepSet0);
    
    let g = new JoinTree()
      .addEdge(e0);
    
    let nodes = g.getNodes();
    let edges = g.getEdges();
    
    expect(nodes.length).to.be.equal(3);
    expect(edges.length).to.be.equal(2);
    
    //TODO more rigorous testing later
  });
  
  it('should not add cycles or invalid edges', () => {
    let n0 = new BbnNode(new Variable(0, 'n0', new Set(['t', 'f'])));
    let n1 = new BbnNode(new Variable(1, 'n1', new Set(['t', 'f'])));
    let n2 = new BbnNode(new Variable(2, 'n2', new Set(['t', 'f'])));
    
    let clique0 = new Clique([n0, n1]);
    let clique1 = new Clique([n1, n2]);
    let sepSet0 = clique0.getSepSet(clique1);
    let sepSet1 = clique0.getSepSet(clique1);
    let sepSet2 = clique1.getSepSet(clique0);
    let sepSet3 = clique0.getSepSet(clique0);
    
    let e0 = new JtEdge(sepSet0);
    let e1 = new JtEdge(sepSet1);
    let e2 = new JtEdge(sepSet2);
    let e3 = new JtEdge(sepSet3);
    
    let g = new JoinTree()
      .addNode(clique0)
      .addNode(clique1)
      .addNode(sepSet0)
      .addEdge(e0)
      .addEdge(e1)
      .addEdge(e2)
      .addEdge(e3);
    
    let nodes = g.getNodes();
    let edges = g.getEdges();
    
    expect(nodes.length).to.be.equal(3);
    expect(edges.length).to.be.equal(2);
    
    //TODO more rigorous testing later
  });
});

describe('testing id util', () => {
  it('should hash string values correctly', () => {
    let s1 = '0--1';
    let s2 = '0--1';
    let s3 = '0->1';
    
    let h1 = IdUtil.hashOfStr(s1);
    let h2 = IdUtil.hashOfStr(s2);
    let h3 = IdUtil.hashOfStr(s3);
    
    expect(h1).to.equal(h2);
    expect(h1).to.not.equal(h3);
  });
  
  it('should hash string array correctly', () => {
    let arr1 = ['a', 'b', 'c'];
    let arr2 = ['b', 'a', 'c'];
    let arr3 = ['c', 'd', 'b'];
    
    let h1 = IdUtil.hashOfArr(arr1);
    let h2 = IdUtil.hashOfArr(arr2);
    let h3 = IdUtil.hashOfArr(arr3);
    
    expect(h1).to.equal(h2);
    expect(h1).to.not.equal(h3);
  });
});