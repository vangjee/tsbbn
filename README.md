# Intro

A TypeScript implementation of exact inference in Bayesian Belief Networks.

# Building, Testing, Distribution

Here are some useful commands to build and test the code.

* `npm run lint` lints the code
* `npm run compile` compiles the code
* `npm run clean` cleans the code
* `npm run test` runs the tests
* `npm run coverage` runs the code coverage analysis
* `npm run dist` creates the distribution packages
* `npm run server` runs a demo of the code

# Run demo Typescript app

Install `ts-node` and `typescript` globally.

```bash
npm install -g ts-node typescript
```

Then run `ts-node src/app.ts`.