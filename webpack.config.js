module.exports = {  
  entry: './src/tsbbn.ts',
  output: {
    filename: './dist/ts-bbn-bundle.js'
  },
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
  },
  devtool: 'inline-source-map',
  module: {
    loaders: [
      { test: /\.ts$/, loader: 'ts-loader' }
    ]
  }
}