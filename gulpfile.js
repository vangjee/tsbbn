'use strict';

var gulp = require('gulp');
var webpack = require('gulp-webpack');
var tsc = require('gulp-typescript');
var tslint = require('gulp-tslint');
var sourcemaps = require('gulp-sourcemaps');
var istanbul = require('gulp-istanbul');
var mocha = require('gulp-mocha');
var babel = require('gulp-babel');
var del = require('del');
var merge = require('merge-stream');
var merge2 = require('merge2');

gulp.task('lint', function () {
  return gulp.
    src('src/**/*.ts')
    .pipe(tslint({ 
      formatter: 'verbose',
      configuration: 'tslint.json'
    }))
    .pipe(tslint.report({
      summarizeFailureOutput: true
    }));
});

gulp.task('compile:app', function () {
  var tsProject = tsc.createProject('tsconfig.json');
  return gulp.src(['./src/app.ts'])
    .pipe(tsProject())
    .pipe(gulp.dest('./dist/src'));
});

gulp.task('compile:es6', function() {
  var tsProject = tsc.createProject('tsconfig.json');
  var tsResult = gulp.src(['./src/**/*.ts', '!./src/app.ts'])
    .pipe(sourcemaps.init())
    .pipe(tsProject());
  return merge2([
    tsResult.dts.pipe(gulp.dest('./dist/defs')),
    tsResult.js
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('./dist/src'))
    ]);
});

gulp.task('compile:es5', ['compile:es6'], function() {
  return gulp.src(['./dist/src/**/*.js'])
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./lib/src'));
});

gulp.task('compile:test', function() {
  var tsProject = tsc.createProject('tsconfig.json');
  var tsResult = gulp.src(['./test/**/*.ts'])
    .pipe(sourcemaps.init())
    .pipe(tsProject());
  return merge(tsResult, tsResult.js)
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/test'));
});

gulp.task('compile', ['compile:es5', 'compile:es6', 'compile:test']);

gulp.task('dist-system', ['lint'], function() {
  var tsResult = 
    gulp.src(['src/**/*.ts', '!src/app.ts'])
      .pipe(sourcemaps.init())
      .pipe(tsc({ 
        out: 'tsbbn-system.js',
        noImplicitAny: true, 
        target: 'es6', 
        sourceMap: true, 
        module: 'system' 
      }));
  return tsResult.js
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist'));
});

gulp.task('dist-amd', ['lint'], function() {
  var tsResult = 
    gulp.src(['src/**/*.ts', '!src/app.ts'])
      .pipe(sourcemaps.init())
      .pipe(tsc({ 
        out: 'tsbbn-amd.js',
        noImplicitAny: true, 
        target: 'es6', 
        sourceMap: true, 
        module: 'amd' 
      }));
  return tsResult.js
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist'));
});

gulp.task('dist-webpack', ['lint', 'compile'], function() {
  return gulp.src('src/pack.js')
    .pipe(webpack({
      output: {
        filename: 'tsbbn-bundle.js'
      },
      resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
      },
      devtool: 'inline-source-map',
      module: {
        loaders: [ { test: /\.ts$/, loader: 'ts-loader' }]
      }
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('dist', ['dist-system', 'dist-amd', 'dist-webpack']);

gulp.task('test', ['compile'], function() {
  var tsProject = tsc.createProject('tsconfig.json');
  return gulp.src('./dist/test/**/*.js')
    .pipe(mocha({
      reporter: 'spec',
      ui: 'bdd'
    }));
});

gulp.task('clean', function (cb) {
  del('./dist', cb);
  del('./coverage', cb);
  del('./lib', cb);
});

gulp.task('watch', function() {
    gulp.watch(['./src/**/*.ts', './test/**/*.ts'], ['lint', 'compile', 'test']);
});

gulp.task('default', ['lint', 'compile', 'test', 'coverage', 'dist']);

gulp.task('pre-coverage', ['test'],  function () {
  return gulp.src(['./dist/src/**/*.js'])
    .pipe(istanbul())
    .pipe(istanbul.hookRequire());
});
 
gulp.task('coverage', ['pre-coverage'], function () {
  return gulp.src(['./dist/test/**/*.js'])
    .pipe(mocha())
    .pipe(istanbul.writeReports())
    .pipe(istanbul.enforceThresholds({ thresholds: { global: 5 } }));
});