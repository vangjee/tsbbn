import {assert, expect} from 'chai';
import {Bbn, BbnNode, BbnUtil, BEdge, EdgeType, EvidenceBuilder, JoinTree, Variable} from '../src/graph';
import {
  InferenceController,
  Initializer,
  Moralizer,
  PotentialInitializer,
  Propagator,
  Transformer,
  Triangulator
} from '../src/pptc';

const validatePosterior = (expected: any, jt: JoinTree): void => {
  jt.getBbnNodes().forEach(node => {
    const o = jt.getBbnPotential(node).entries.map(e => e.value);
    const e = expected[node.variable.name];

    expect(o.length).to.be.equal(e.length);
    for (let i = 0; i < o.length; i++) {
      const diff = Math.abs(o[i] - e[i]);
      assert.isTrue(diff < 0.001)
    }
  });
}

const printPotentials = (jt: JoinTree): void => {
  jt.getBbnNodes().forEach(node => {
    const p = jt.getBbnPotential(node).entries
      .map(e => e.toString())
      .map(s => {
        const tokens = s.split(',');
        const prob = Number(tokens[1]).toFixed(5);
        const val = tokens[0].split('=')[1];
        return `${val}=${prob}`;
      })
      .join(', ');
    const s = `${node.variable.name}: ${p}`;
    console.log(s);
  })
}

describe('test inference controller', () => {
  it('should do inference on huang graph correctly', () => {
    let bbn = BbnUtil.getHuangGraph();
    let jt = InferenceController.apply(bbn);

    validatePosterior({
      'a': [0.5, 0.5],
      'b': [0.45, 0.55],
      'c': [0.45, 0.55],
      'd': [0.68, 0.32],
      'e': [0.465, 0.535],
      'f': [0.176, 0.824],
      'g': [0.415, 0.585],
      'h': [0.823, 0.177]
    }, jt);
  });
  it('should do inference on graph1 correctly', () => {
    const difficulty = new BbnNode(new Variable(0, 'difficulty', new Set<string>(['easy', 'hard'])), [0.6, 0.4]);
    const intelligence = new BbnNode(new Variable(1, 'intelligence', new Set<string>(['low', 'high'])), [0.7, 0.3]);
    const grade = new BbnNode(new Variable(2, 'grade', new Set<string>(['a', 'b', 'c'])), [0.3, 0.4, 0.3, 0.9, 0.08, 0.02, 0.05, 0.25, 0.7, 0.5, 0.3, 0.2]);
    const sat = new BbnNode(new Variable(3, 'sat', new Set<string>(['low', 'high'])), [0.95, 0.05, 0.2, 0.8]);
    const letter = new BbnNode(new Variable(4, 'letter', new Set<string>(['weak', 'strong'])), [0.1, 0.9, 0.4, 0.6, 0.99, 0.01]);

    const bbn = new Bbn()
      .addNode(difficulty)
      .addNode(intelligence)
      .addNode(grade)
      .addNode(sat)
      .addNode(letter)
      .addEdge(new BEdge(difficulty, grade, EdgeType.DIRECTED))
      .addEdge(new BEdge(intelligence, grade, EdgeType.DIRECTED))
      .addEdge(new BEdge(intelligence, sat, EdgeType.DIRECTED))
      .addEdge(new BEdge(grade, letter, EdgeType.DIRECTED)) as Bbn;

    let jt = InferenceController.apply(bbn);

    validatePosterior({
      'difficulty': [0.6, 0.4],
      'intelligence': [0.7, 0.3],
      'grade': [0.362, 0.288, 0.350],
      'sat': [0.725, 0.275],
      'letter': [0.498, 0.502]
    }, jt);
    
    let ev = new EvidenceBuilder()
      .withNode(jt.getBbnNodeByName('sat'))
      .withEvidence('high', 1.0)
      .build();
    jt.unobserveAll();
    jt.setObservation(ev);
  
    validatePosterior({
      'difficulty': [0.6, 0.4],
      'intelligence': [0.127, 0.873],
      'grade': [0.671, 0.190, 0.139],
      'sat': [0.0, 1.0],
      'letter': [0.281, 0.719]
    }, jt);
  
    ev = new EvidenceBuilder()
      .withNode(jt.getBbnNodeByName('sat'))
      .withEvidence('low', 1.0)
      .build();
    jt.unobserveAll();
    jt.setObservation(ev);
  
    validatePosterior({
      'difficulty': [0.6, 0.4],
      'intelligence': [0.917, 0.0828],
      'grade': [0.245, 0.326, 0.430],
      'sat': [1.0, 0.0],
      'letter': [0.58, 0.42]
    }, jt);
  });
  it('should do inference on trivial graphs correctly', () => {
    const a1 = new BbnNode(new Variable(0, 'a', new Set<string>(['t', 'f'])), [0.2, 0.8]);
    const b1 = new BbnNode(new Variable(1, 'b', new Set<string>(['t', 'f'])), [0.1, 0.9, 0.9, 0.1]);
    const bbn1 = new Bbn()
      .addNode(a1)
      .addNode(b1)
      .addEdge(new BEdge(a1, b1, EdgeType.DIRECTED)) as Bbn;
    const jt1 = InferenceController.apply(bbn1);
    validatePosterior({
      'a': [0.2, 0.8],
      'b': [0.74, 0.26]
    }, jt1);
  
    const a2 = new BbnNode(new Variable(1, 'a', new Set<string>(['t', 'f'])), [0.2, 0.8]);
    const b2 = new BbnNode(new Variable(0, 'b', new Set<string>(['t', 'f'])), [0.1, 0.9, 0.9, 0.1]);
    const bbn2 = new Bbn()
      .addNode(a2)
      .addNode(b2)
      .addEdge(new BEdge(a2, b2, EdgeType.DIRECTED)) as Bbn;
    const jt2 = InferenceController.apply(bbn2);
    validatePosterior({
      'a': [0.2, 0.8],
      'b': [0.74, 0.26]
    }, jt1);
  
    const a3 = new BbnNode(new Variable(0, 'a', new Set<string>(['t', 'f'])), [0.2, 0.8]);
    const b3 = new BbnNode(new Variable(1, 'b', new Set<string>(['t', 'f'])), [0.1, 0.9]);
    const bbn3 = new Bbn()
      .addNode(a3)
      .addNode(b3) as Bbn;
    const jt3 = InferenceController.apply(bbn3);
    validatePosterior({
      'a': [0.2, 0.8],
      'b': [0.1, 0.9]
    }, jt3);
  });
  it('should do inference on permuted graphs nodes', () => {
    const a1 = new BbnNode(new Variable(0, 'a', new Set<string>(['t', 'f'])), [0.2, 0.8]);
    const b1 = new BbnNode(new Variable(1, 'b', new Set<string>(['t', 'f'])), [0.1, 0.9, 0.9, 0.1]);
    const c1 = new BbnNode(new Variable(2, 'c', new Set<string>(['t', 'f'])), [0.2, 0.8, 0.7, 0.3]);
    const bbn1 = new Bbn()
      .addNode(a1)
      .addNode(b1)
      .addNode(c1)
      .addEdge(new BEdge(a1, b1, EdgeType.DIRECTED))
      .addEdge(new BEdge(b1, c1, EdgeType.DIRECTED))as Bbn;
    const jt1 = InferenceController.apply(bbn1);
    validatePosterior({
      'a': [0.2, 0.8],
      'b': [0.74, 0.26],
      'c': [0.33, 0.67]
    }, jt1);
  
  
    const a2 = new BbnNode(new Variable(0, 'a', new Set<string>(['t', 'f'])), [0.2, 0.8]);
    const b2 = new BbnNode(new Variable(1, 'b', new Set<string>(['t', 'f'])), [0.1, 0.9, 0.9, 0.1]);
    const c2 = new BbnNode(new Variable(2, 'c', new Set<string>(['t', 'f'])), [0.2, 0.8, 0.7, 0.3]);
    const bbn2 = new Bbn()
      .addNode(a2)
      .addNode(b2)
      .addNode(c2)
      .addEdge(new BEdge(a2, b2, EdgeType.DIRECTED))
      .addEdge(new BEdge(b2, c2, EdgeType.DIRECTED))as Bbn;
    const jt2 = InferenceController.apply(bbn2);
    validatePosterior({
      'a': [0.2, 0.8],
      'b': [0.74, 0.26],
      'c': [0.33, 0.67]
    }, jt2);
  });
  it('should toggle observation', () => {
    let bbn = BbnUtil.getHuangGraph();
    let joinTree = InferenceController.apply(bbn);
    
    // console.log('INIT');
    // joinTree.getBbnNodes().forEach(node => {
    //   let potential = joinTree.getBbnPotential(node);
    //   console.log(node.toString());
    //   console.log(potential.toString());
    //   let total = 0.0;
    //   potential.entries.forEach(e => total += e.value);
    //   console.log('total = ' + total);
    //   console.log('-------');
    // });
    
    let ev0 = new EvidenceBuilder()
      .withNode(joinTree.getBbnNode(0))
      .withEvidence('on', 1.0)
      .build();
    
    joinTree.setObservation(ev0);
    
    // console.log('FIRST');
    // joinTree.getBbnNodes().forEach(node => {
    //   let potential = joinTree.getBbnPotential(node);
    //   console.log(node.toString());
    //   console.log(potential.toString());
    //   let total = 0.0;
    //   potential.entries.forEach(e => total += e.value);
    //   console.log('total = ' + total);
    //   console.log('-------');
    // });
    
    joinTree.setObservation(ev0);
    
    // console.log('SECOND');
    // joinTree.getBbnNodes().forEach(node => {
    //   let potential = joinTree.getBbnPotential(node);
    //   console.log(node.toString());
    //   console.log(potential.toString());
    //   let total = 0.0;
    //   potential.entries.forEach(e => total += e.value);
    //   console.log('total = ' + total);
    //   console.log('-------');
    // });
    
    joinTree.setObservation(ev0);
    
    // console.log('THIRD');
    // joinTree.getBbnNodes().forEach(node => {
    //   let potential = joinTree.getBbnPotential(node);
    //   console.log(node.toString());
    //   console.log(potential.toString());
    //   let total = 0.0;
    //   potential.entries.forEach(e => total += e.value);
    //   console.log('total = ' + total);
    //   console.log('-------');
    // });
  });
  it('should perform observation inference', () => {
    let bbn = BbnUtil.getHuangGraph();
    let joinTree = InferenceController.apply(bbn);
    
    // joinTree.getBbnNodes().forEach(node => {
    //   let potential = joinTree.getBbnPotential(node);
    //   console.log(node.toString());
    //   console.log(potential.toString());
    //   let total = 0.0;
    //   potential.entries.forEach(e => total += e.value);
    //   console.log('total = ' + total);
    //   console.log('-------');
    // });
    
    let ev0 = new EvidenceBuilder()
      .withNode(joinTree.getBbnNode(0))
      .withEvidence('on', 1.0)
      .build();
    
    joinTree.updateEvidences([ev0]);
    
    // console.log('******');
    // console.log('******');
    // console.log('******');
    
    // joinTree.getBbnNodes().forEach(node => {
    //   let potential = joinTree.getBbnPotential(node);
    //   console.log(node.toString());
    //   console.log(potential.toString());
    //   let total = 0.0;
    //   potential.entries.forEach(e => total += e.value);
    //   console.log('total = ' + total);
    //   console.log('-------');
    // });
    
    let ev1 = new EvidenceBuilder()
      .withNode(joinTree.getBbnNodeByName('a'))
      .withEvidence('off', 1.0)
      .build();
    joinTree.updateEvidences([ev1]);
    
    // console.log('NEXT 1');
    
    // joinTree.getBbnNodes().forEach(node => {
    //   let potential = joinTree.getBbnPotential(node);
    //   console.log(node.toString());
    //   console.log(potential.toString());
    //   let total = 0.0;
    //   potential.entries.forEach(e => total += e.value);
    //   console.log('total = ' + total);
    //   console.log('-------');
    // });
    
    // console.log('NEXT 2');
    joinTree.unobserveAll();
    
    // joinTree.getBbnNodes().forEach(node => {
    //   let potential = joinTree.getBbnPotential(node);
    //   console.log(node.toString());
    //   console.log(potential.toString());
    //   let total = 0.0;
    //   potential.entries.forEach(e => total += e.value);
    //   console.log('total = ' + total);
    //   console.log('-------');
    // });
    
    // console.log('NEXT 2');
    let ev2 = new EvidenceBuilder()
      .withNode(joinTree.getBbnNodeByName('a'))
      .withEvidence('on', 1.0)
      .build();
    let ev3 = new EvidenceBuilder()
      .withNode(joinTree.getBbnNodeByName('f'))
      .withEvidence('on', 1.0)
      .build();
    
    joinTree.updateEvidences([ev2, ev3]);
    // joinTree.getBbnNodes().forEach(node => {
    //   let potential = joinTree.getBbnPotential(node);
    //   console.log(node.toString());
    //   console.log(potential.toString());
    //   let total = 0.0;
    //   potential.entries.forEach(e => total += e.value);
    //   console.log('total = ' + total);
    //   console.log('-------');
    // });
    
    // TODO test later
  });
});

describe('test Initializer', () => {
  it('should initialize', () => {
    let bbn = BbnUtil.getHuangGraph();
    PotentialInitializer.init(bbn);
    
    let ug = Moralizer.moralize(bbn);
    let cliques = Triangulator.triangulate(ug);
    let joinTree = Transformer.transform(cliques);
    Initializer.initialize(joinTree);
    
    // console.log('there are ' + joinTree.getNodes().length + ' nodes');
    // joinTree.getNodes().forEach(n => console.log((n as Clique).toString()));
    
    // console.log('there are ' + joinTree.getEdges().length + ' edges');
    // joinTree.getEdges().forEach(e => {
    //   let lhs = joinTree.getNode(e.lhs.id) as Clique;
    //   let rhs = joinTree.getNode(e.rhs.id) as Clique;
    //   console.log(lhs.toString() + '--' + rhs.toString());
    // });
    
    // joinTree.getCliques().forEach(clique => {
    //   let potential = joinTree.potentials.get(clique.id);
    //   console.log(clique.toString());
    //   console.log(potential.toString());
    // });
    
    // TODO test later;
  });
});

describe('test moralizer', () => {
  it('should moralize', () => {
    let bbn = BbnUtil.getHuangGraph();
    PotentialInitializer.init(bbn);
    
    let ug = Moralizer.moralize(bbn);
    
    // ug.getNodes().forEach(node => console.log(node));
    // ug.getEdges().forEach(edge => console.log(edge));
    
    // TODO test later; only 2 new edges added; 3--4 and 4--6
  });
});

describe('testing potential initializer', () => {
  it('should initialize the potential of a bbn', () => {
    let bbn = BbnUtil.getHuangGraph();
    
    // PotentialInitializer.init(bbn);
    
    // bbn.getNodes().forEach(n => {
    //   let bbnNode = (n as BbnNode);
    //   let potential = bbnNode.potential
    //   console.log(bbnNode.id + ' ' + bbnNode.variable.name);
    //   console.log(potential.toString());
    // })
    
    // TODO: assert later
  });
});

describe('test propagator', () => {
  it('should propagate', () => {
    let bbn = BbnUtil.getHuangGraph();
    PotentialInitializer.init(bbn);
    
    let ug = Moralizer.moralize(bbn);
    let cliques = Triangulator.triangulate(ug);
    let joinTree = Transformer.transform(cliques);
    Initializer.initialize(joinTree);
    
    // console.log('after initilization'.toUpperCase());
    // joinTree.getNodes().forEach(node => {
    //   let potential = joinTree.potentials.get(node.id);
    //   console.log(node.toString());
    //   console.log(potential.toString());
    //   let total = 0.0;
    //   potential.entries.forEach(e => total += e.value);
    //   console.log('total = ' + total);
    // });
    
    Propagator.propagate(joinTree);
    
    
    // console.log('there are ' + joinTree.getNodes().length + ' nodes');
    // joinTree.getNodes().forEach(n => console.log((n as Clique).toString()));
    
    // console.log('there are ' + joinTree.getEdges().length + ' edges');
    // joinTree.getEdges().forEach(e => {
    //   let lhs = joinTree.getNode(e.lhs.id) as Clique;
    //   let rhs = joinTree.getNode(e.rhs.id) as Clique;
    //   console.log(lhs.toString() + '--' + rhs.toString());
    // });
    
    // console.log('after propagation'.toUpperCase());
    joinTree.getNodes().forEach(node => {
      let potential = joinTree.potentials.get(node.id);
      // console.log(node.toString());
      // console.log(potential.toString());
      let total = 0.0;
      potential.entries.forEach(e => total += e.value);
      // console.log('total = ' + total);
      expect(total).to.be.closeTo(1, 0.01);
    });
  });
});

describe('test transformer', () => {
  it('should transform', () => {
    let bbn = BbnUtil.getHuangGraph();
    PotentialInitializer.init(bbn);
    
    let ug = Moralizer.moralize(bbn);
    let cliques = Triangulator.triangulate(ug);
    let joinTree = Transformer.transform(cliques);
    
    // console.log('there are ' + joinTree.getNodes().length + ' nodes');
    // joinTree.getNodes().forEach(n => console.log((n as Clique).toString()));
    
    // console.log('there are ' + joinTree.getEdges().length + ' edges');
    // joinTree.getEdges().forEach(e => {
    //   let lhs = joinTree.getNode(e.lhs.id) as Clique;
    //   let rhs = joinTree.getNode(e.rhs.id) as Clique;
    //   console.log(lhs.toString() + '--' + rhs.toString());
    // });
    
    // TODO test later;
  });
});

describe('test triangulator', () => {
  it('should triangulate', () => {
    let bbn = BbnUtil.getHuangGraph();
    PotentialInitializer.init(bbn);
    
    let ug = Moralizer.moralize(bbn);
    let cliques = Triangulator.triangulate(ug);
    
    // cliques.forEach(c => console.log(c.toString()));
    
    // TODO test later;
  });
});