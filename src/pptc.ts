import {
  Bbn,
  BbnNode,
  BEdge,
  BNode,
  Clique,
  CompareUtil,
  Dag,
  JoinTree,
  JoinTreeListener,
  JtEdge,
  PotentialUtil,
  SepSet,
  Ug
} from './graph';

export class InferenceController implements JoinTreeListener {
  public static apply(bbn: Bbn): JoinTree {
    PotentialInitializer.init(bbn);

    const ug = Moralizer.moralize(bbn);
    const cliques = Triangulator.triangulate(ug);
    const joinTree = Transformer.transform(cliques);

    Initializer.initialize(joinTree);
    Propagator.propagate(joinTree);

    joinTree.setListener(new InferenceController());

    return joinTree;
  }

  public evidenceRetracted(jointree: JoinTree) {
    Initializer.initialize(jointree);
    Propagator.propagate(jointree);
  }

  public evidenceUpdated(jointree: JoinTree) {
    Propagator.propagate(jointree);
  }
}

export class Initializer {
  public static initialize(joinTree: JoinTree): JoinTree {
    joinTree.getCliques().forEach(clique => {
      const potential = PotentialUtil.getPotentialFromNodes(clique.nodes);
      joinTree.addPotential(clique, potential);
    });

    joinTree.getSepSets().forEach(sepSet => {
      const potential = PotentialUtil.getPotentialFromNodes(sepSet.nodes);
      joinTree.addPotential(sepSet, potential);
    });

    joinTree.getBbnNodes().forEach(node => {
      const clique = Initializer.getClique(node, joinTree);
      // console.log(node.variable.name + ' mapped to clique ' + clique.toString());
      const p1 = joinTree.potentials.get(clique.id);
      const p2 = node.potential;
      // console.log(p1.toString());
      // console.log('>>>>');
      // console.log(p2.toString());
      // console.log('----');
      PotentialUtil.multiply(p1, p2);
      // console.log(p1.toString());
      // console.log('****');
    });

    joinTree.getBbnNodes().forEach(node => {
      node.variable.values.forEach(value => {
        const clique = node.metadata.get('parent.clique') as Clique;
        const cliquePotential = joinTree.potentials.get(clique.id);
        const nodePotential = joinTree.getEvidence(node, value);
        PotentialUtil.multiply(cliquePotential, nodePotential);
        // console.log(clique.toString());
        // console.log(cliquePotential.toString());
      });
    });

    return joinTree;
  }

  private static getClique(node: BbnNode, jointree: JoinTree): Clique {
    let clique: Clique;

    if (!node.metadata.has('parent.clique')) {
      const cliques = jointree.findCliquesWithNodeAndParents(node.id)
          .sort((a, b) => CompareUtil.intCompare(a.id, b.id));
      clique = cliques[0];
    } else {
      clique = node.metadata.get('parent.clique') as Clique;
    }
    node.addMetadata('parent.clique', clique);

    return clique;
  }
}

export class Moralizer {
  public static moralize(dag: Dag): Ug {
    const ug = new Ug();
    dag.getNodes().forEach(node => ug.addNode(node));
    dag.getEdges().forEach(edge => ug.addEdge(new BEdge(edge.lhs, edge.rhs)));
    dag.getNodes().forEach(node => {
      const parents = Array.from(dag.getParents(node.id));
      const size = parents.length;
      for (let i = 0; i < size; i++) {
        const pa1 = dag.getNode(parents[i]);
        for (let j = i + 1; j < size; j++) {
          const pa2 = dag.getNode(parents[j]);
          ug.addEdge(new BEdge(pa1, pa2));
        }
      }
    });
    dag.getNodes().forEach(node => {
      const parents = Array.from(dag.getParents(node.id)).map(id => dag.getNode(id));
      node.addMetadata('parents', parents);
    });
    return ug;
  }
}

export class PotentialInitializer {
  public static init(bbn: Bbn) {
    bbn.getNodes().forEach(node => {
      const bbnNode = node as BbnNode;
      const parents = Array.from(bbn.getParents(node.id)).map(id => bbn.getNode(id) as BbnNode);
      bbnNode.potential = PotentialUtil.getPotential(bbnNode, parents);
    });
  }
}

export class Propagator {
  public static propagate(joinTree: JoinTree): JoinTree {
    const cliques = joinTree.getCliques().sort((a, b) => CompareUtil.intCompare(a.id, b.id));
    const x = cliques[0];
    // console.log(x.toString());

    joinTree.unmarkCliques();
    this.collectEvidence(joinTree, x);

    joinTree.unmarkCliques();
    this.distributeEvidence(joinTree, x);

    return joinTree;
  }

  private static collectEvidence(jointree: JoinTree, start: Clique) {
    const collector = new EvidenceCollector(jointree, start);
    collector.start();
  }

  private static distributeEvidence(jointree: JoinTree, start: Clique) {
    const distributor = new EvidenceDistributor(jointree, start);
    distributor.start();
  }
}

export class Transformer {
  public static transform(cliques: Clique[]): JoinTree {
    const jointree = new JoinTree();
    cliques.forEach(c => jointree.addNode(c));

    const sepSets = this.getSepSets(cliques);
    sepSets.forEach(s => jointree.addEdge(new JtEdge(s)));

    return jointree;
  }

  private static getSepSets(cliques: Clique[]): SepSet[] {
    const sepSets: SepSet[] = [];
    const size = cliques.length;
    for (let i = 0; i < size; i++) {
      for (let j = i + 1; j < size; j++) {
        const sepSet = new SepSet(cliques[i], cliques[j]);
        if (!sepSet.isEmpty()) {
          sepSets.push(sepSet);
        }
      }
    }
    return sepSets.sort((a, b) => {
      let result = -1 * CompareUtil.intCompare(a.getMass(), b.getMass());
      if (0 === result) {
        result = CompareUtil.intCompare(a.getCost(), b.getCost());
        if (0 === result) {
          result = CompareUtil.intCompare(a.id, b.id);
        }
      }
      return result;
    });
  }
}

export class Triangulator {
  public static triangulate(m: Ug): Clique[] {
    const cliques: Clique[] = [];
    const mm = this.duplicate(m);
    while (mm.getNodes().length > 0) {
      const nodeClique = this.selectNode(mm);

      const clique = new Clique(nodeClique.getBbnNodes());

      if (!this.isSubset(cliques, clique)) {
        cliques.push(clique);
      }

      mm.removeNode(nodeClique.node.id);

      nodeClique.edges.forEach(e => {
        m.addEdge(e);
        mm.addEdge(e);
      });
    }
    return cliques;
  }

  private static duplicate(g: Ug): Ug {
    const ug = new Ug();
    g.getNodes().forEach(n => ug.addNode(n));
    g.getEdges().forEach(e => ug.addEdge(e));
    return ug;
  }

  private static selectNode(m: Ug): NodeClique {
    const cliques = m.getNodes()
        .map(node => {
          const weight = Triangulator.getWeight(node, m);
          const edges = Triangulator.getEdgesToAdd(node, m);
          const neighbors = Array.from(m.getNeighbors(node.id)).map(id => m.getNode(id));
          return new NodeClique(node, neighbors, weight, edges);
        })
        .sort((a, b) => {
          let result = CompareUtil.intCompare(a.edges.length, b.edges.length);
          if (0 === result) {
            result = CompareUtil.intCompare(a.weight, b.weight);
            if (0 === result) {
              result = CompareUtil.intCompare(a.node.id, b.node.id);
            }
          }
          return result;
        });
    return cliques[0];
  }

  private static getWeight(n: BNode, m: Ug) {
    let weight = (n as BbnNode).getWeight();
    Array.from(m.getNeighbors(n.id))
        .map(id => m.getNode(id))
        .forEach(neighbor => weight *= (neighbor as BbnNode).getWeight());
    return weight;
  }

  private static getEdgesToAdd(n: BNode, m: Ug): BEdge[] {
    const edges: BEdge[] = [];
    const neighbors = Array.from(m.getNeighbors(n.id)).map(id => m.getNode(id));
    const size = neighbors.length;
    for (let i = 0; i < size; i++) {
      const ne1 = neighbors[i];
      for (let j = 0; j < size; j++) {
        const ne2 = neighbors[j];
        if (!m.edgeExists(ne1.id, ne2.id)) {
          edges.push(new BEdge(ne1, ne2));
        }
      }
    }
    return edges;
  }

  private static isSubset(cliques: Clique[], clique: Clique): boolean {
    for (let i = 0; i < cliques.length; i++) {
      if (cliques[i].isSuperset(clique)) {
        return true;
      }
    }
    return false;
  }
}

class NodeClique {
  constructor(public node: BNode, public neighbors: BNode[], public weight: number, public edges: BEdge[]) {

  }

  getBbnNodes(): BbnNode[] {
    const nodes = this.neighbors.slice(0);
    nodes.push(this.node);
    return nodes.map(n => (n as BbnNode));
  }
}

export class EvidenceCollector {
  constructor(public joinTree: JoinTree, public startClique: Clique) {

  }

  public start() {
    // console.log('STARTING EVIDENCE COLLECTION from ' + this.startClique.toString());

    this.startClique.mark();
    this.joinTree.getNeighbors(this.startClique.id).forEach(sepSetId => {
      const sepSet = this.joinTree.getNode(sepSetId) as SepSet;
      Array.from(this.joinTree.getNeighbors(sepSetId))
          .map(id => this.joinTree.getNode(id) as Clique)
          .filter(clique => !clique.isMarked())
          .forEach(y => {
            this.walk(this.startClique, sepSet, y);
          });
    });
  }

  private walk(x: Clique, s: SepSet, y: Clique) {
    y.mark();
    Array.from(this.joinTree.getNeighbors(y.id))
        .map(id => this.joinTree.getNode(id) as SepSet)
        .forEach(sepSet => {
          Array.from(this.joinTree.getNeighbors(sepSet.id))
              .map(id => this.joinTree.getNode(id) as Clique)
              .filter(clique => !clique.isMarked())
              .forEach(clique => this.walk(y, sepSet, clique));
        });
    PotentialUtil.passSingleMessage(this.joinTree, y, s, x);
  }
}

export class EvidenceDistributor {
  constructor(public joinTree: JoinTree, public startClique: Clique) {

  }

  public start() {
    // console.log('STARTING EVIDENCE DISTRIBUTION from ' + this.startClique.toString());

    this.startClique.mark();
    this.joinTree.getNeighbors(this.startClique.id).forEach(sepSetId => {
      const sepSet = this.joinTree.getNode(sepSetId) as SepSet;
      Array.from(this.joinTree.getNeighbors(sepSetId))
          .map(id => this.joinTree.getNode(id) as Clique)
          .filter(clique => !clique.isMarked())
          .forEach(y => {
            PotentialUtil.passSingleMessage(this.joinTree, this.startClique, sepSet, y);
            this.walk(this.startClique, sepSet, y);
          });
    });
  }

  private walk(x: Clique, s: SepSet, y: Clique) {
    y.mark();
    Array.from(this.joinTree.getNeighbors(y.id))
        .map(id => this.joinTree.getNode(id) as SepSet)
        .forEach(sepSet => {
          Array.from(this.joinTree.getNeighbors(sepSet.id))
              .map(id => this.joinTree.getNode(id) as Clique)
              .filter(clique => !clique.isMarked())
              .forEach(clique => {
                PotentialUtil.passSingleMessage(this.joinTree, y, sepSet, clique);
                this.walk(y, sepSet, clique);
              });
        });
  }
}
