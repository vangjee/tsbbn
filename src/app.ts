import {BbnUtil, EvidenceBuilder} from './graph';
import {InferenceController} from './pptc';

let bbn = BbnUtil.getHuangGraph();
let joinTree = InferenceController.apply(bbn);

let ev = new EvidenceBuilder().withNode(joinTree.getBbnNodeByName('a')).withEvidence('on', 1.0).build();
joinTree.setObservation(ev);
joinTree.setObservation(ev);

let ev2 = new EvidenceBuilder()
  .withNode(joinTree.getBbnNodeByName('a'))
  .withEvidence('on', 1.0)
  .build();
let ev3 = new EvidenceBuilder()
  .withNode(joinTree.getBbnNodeByName('f'))
  .withEvidence('on', 1.0)
  .build();
joinTree.updateEvidences([ev2, ev3]);

joinTree.getBbnNodes().forEach(node => {
  let potential = joinTree.getBbnPotential(node);
  console.log(node.toString());
  console.log(potential.toString());
  let total = 0.0;
  potential.entries.forEach(e => total += e.value);
  console.log('total = ' + total);
});